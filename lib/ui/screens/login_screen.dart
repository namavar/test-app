import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app/ui/screens/home_screen.dart';
import 'package:test_app/api/post.dart';
import 'package:test_app/components/app_button.dart';
import 'package:test_app/components/custom_text_field.dart';
import 'package:test_app/components/login_component.dart';
import 'package:test_app/components/logo_widget.dart';
import 'package:test_app/utils/colors.dart';
import 'package:test_app/utils/routes.dart';
import 'package:test_app/utils/screen_util.dart';
import 'package:test_app/utils/strings.dart';
import 'package:test_app/utils/text_styles.dart';
import 'package:test_app/utils/validator.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey =
      new GlobalObjectKey<ScaffoldState>('LoginScreen');

  String _email = "";
  String _password = "";

  bool _screenUtilActive = true;

  final _loginFormKey = GlobalKey<FormState>();

  FocusNode _emailFocusNode;
  FocusNode _passwordFocusNode;

  @override
  void initState() {
    super.initState();
    _emailFocusNode = new FocusNode();
    _passwordFocusNode = new FocusNode();
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    super.dispose();
  }

  setScreenSize() {
    if (!_screenUtilActive)
      Constant.setScreenAwareConstant(context);
    else
      Constant.setDefaultSize(context);
    setState(() {
      _screenUtilActive = !_screenUtilActive;
    });
  }

  Map<String, dynamic> toJson() => {
        'email': this._email,
        'password': this._password,
      };

  sendData() {
    _loginFormKey.currentState.save();
    AppApi.fetchLogin(toJson()).then((response) {
      Future.delayed(Duration(milliseconds: 0), () {
        AppRoutes.push(context, HomeScreen(data: response));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        body: Form(
          key: _loginFormKey,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              BackgroundImageWidget(
                color: AppColors.primary,
              ),
              ListView(
                children: <Widget>[
                  Center(
                    child: Opacity(
                      opacity: 0.40,
                      child: AppLogoWidget(
                        margin: EdgeInsets.only(top: Constant.sizeXL),
                        padding: Constant.spacingAllSmall,
                      ),
                    ),
                  ),
                  Container(
                    height: Constant.sizeXXL,
                  ),
                  Container(
                    height: Constant.sizeSmall,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                        vertical: Constant.sizeExtraSmall,
                        horizontal: Constant.screenWidthTenth),
                    child: Text(
                      AppStrings.loginTitle,
                      textAlign: TextAlign.center,
                      style: TextStyles.loginTitle,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                        vertical: Constant.sizeExtraSmall,
                        horizontal: Constant.screenWidthTenth),
                    child: Text(
                      AppStrings.loginSubTitle,
                      textAlign: TextAlign.center,
                      style: TextStyles.loginTitle,
                    ),
                  ),
                  Container(
                    height: Constant.sizeXL,
                  ),
                  Container(
                    height: Constant.sizeXL,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: Constant.screenWidthTenth,
                    ),
                    child: AppTextFormField(
                      focusNode: _emailFocusNode,
                      lableText: AppLabels.email,
                      hintText: AppStrings.enterEmail,
                      validator: FieldValidator.validateEmail,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      onSaved: (value) => this._email = value,
//                      onFieldSubmitted: (_) => setScreenSize(),
                    ),
                  ),
                  Container(
                    height: Constant.sizeMedium,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: Constant.screenWidthTenth,
                    ),
                    child: AppTextFormField(
                      focusNode: _passwordFocusNode,
                      lableText: AppLabels.password,
                      hintText: AppStrings.enterPassword,
                      validator: FieldValidator.validatePassword,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      onSaved: (value) => this._password = value,
//                      onFieldSubmitted: (_) => setScreenSize(),
                    ),
                  ),
                  Container(
                    height: Constant.sizeXXXL,
                  ),
                  Container(
                    height: Constant.sizeXXXL,
                  ),
                  Container(
                    height: Constant.sizeXL,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: Constant.screenWidthTenth,
                    ),
                    child: AppButton(
                      onTap: () => sendData(),
                      text: AppLabels.Login,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
