import 'package:flutter/material.dart';
import 'package:test_app/components/login_component.dart';
import 'package:test_app/components/logo_widget.dart';
import 'package:test_app/ui/screens/home_screen.dart';
import 'package:test_app/ui/screens/login_screen.dart';
import 'package:test_app/utils/colors.dart';
import 'package:test_app/utils/routes.dart';
import 'package:test_app/utils/screen_util.dart';
import 'package:test_app/utils/strings.dart';
import 'package:test_app/utils/text_styles.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 1000), () {
      AppRoutes.makeFirst(context, LoginScreen  ());
    });
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);

    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: <Widget>[
//            BackgroundImageWidget(
//              color: AppColors.secondary,
//            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(child: AppLogoWidget()),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Constant.screenWidthTenth),
                  child: Text(
                    AppStrings.appName,
                    style: TextStyles.splashText,
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
