import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:test_app/models/login.dart';
import 'package:test_app/components/app_bar_widget.dart';
import 'package:test_app/utils/colors.dart';
import 'package:test_app/utils/text_styles.dart';


class HomeScreen extends StatefulWidget {

  //   Declare a field that holds the Person data
  final Login data;

  // In the constructor, require a Person
  HomeScreen({Key key, @required this.data}) : super(key: key);

//  widget.data.id

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
//  GlobalKey<ScaffoldState> _scaffoldKey =
//      new GlobalObjectKey<ScaffoldState>('_HomeScreenState');
  final SnackBar snackBar = const SnackBar(content: Text('Showing Snackbar'));


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.white_smoke,
//      key: _scaffoldKey,
      appBar: AppBarWidget(),
      body: Center(
            child:Column(
              children: <Widget>[
                Text(
                  widget.data.name,
                  style: TextStyles.titleListViewText,
                ),
                Text(
                  widget.data.email,
                  style: TextStyles.titleListViewText,
                )
              ],
            )
          ),
    ));
  }
}

