import 'package:test_app/utils/strings.dart';

class FieldValidator {


  static String validateEmail(String value) {
    print("validateEmail : $value ");

    if (value.isEmpty) return AppStrings.enterEmail;

    Pattern pattern =
        r'/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/';

    RegExp regex = new RegExp(pattern);

    if (!regex.hasMatch(value.trim())) {
      return AppStrings.enterValidEmail;
    }

    return null;
  }

  /// Password matching expression. Password must be at least 4 characters,
  /// no more than 8 characters, and must include at least one upper case letter,
  /// one lower case letter, and one numeric digit.
  static String validatePassword(String value) {
    print("validateEmail : $value ");

    if (value.isEmpty) return AppStrings.enterPassword;

    Pattern pattern = r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$';

    RegExp regex = new RegExp(pattern);

    if (!regex.hasMatch(value.trim())) {
      return AppStrings.enterValidPassword;
    }

    return null;
  }
}
