import 'package:flutter/material.dart';
import 'package:test_app/utils/colors.dart';
import 'package:test_app/utils/screen_util.dart';

class FontFamily {
  static String iranYekan = "IRANYekan";
}

class TextStyles {
  static TextStyle get snackBarText => TextStyle(
        color: Colors.white,
        letterSpacing: 1.4,
        inherit: false,
      );

  static TextStyle get appName => TextStyle(
        fontFamily: FontFamily.iranYekan,
        fontWeight: FontWeight.w700,
        fontSize: FontSize.s25,
        color: Colors.white,
        letterSpacing: 0,
        inherit: false,
      );

  static TextStyle get editText => TextStyle(
        fontFamily: FontFamily.iranYekan,
        fontSize: FontSize.s16,
        color: Colors.black,
        inherit: false,
        textBaseline: TextBaseline.alphabetic,
        letterSpacing: 3.0,
      );

  static TextStyle get labelStyle => TextStyle(
        fontFamily: FontFamily.iranYekan,
        fontWeight: FontWeight.w700,
        fontSize: FontSize.s18,
        color: AppColors.input_text,
        inherit: false,
      );

  static TextStyle get hintStyle => TextStyle(
        fontFamily: FontFamily.iranYekan,
        fontWeight: FontWeight.w700,
        fontSize: FontSize.s12,
        color: AppColors.input_text,
        inherit: false,
      );

  static TextStyle get errorStyle => TextStyle(
        fontSize: FontSize.s12,
        color: Colors.red,
        inherit: false,
      );

  static TextStyle get buttonText => TextStyle(
        fontFamily: FontFamily.iranYekan,
        fontWeight: FontWeight.w700,
        fontSize: FontSize.s20,
        color: Colors.white,
        inherit: false,
      );

  static TextStyle get loginTitle => TextStyle(
        fontFamily: FontFamily.iranYekan,
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w700,
        color: AppColors.login_text,
        inherit: false,
      );

  static TextStyle get loginSubTitle => TextStyle(
        fontSize: FontSize.s14,
        color: Colors.grey,
        inherit: false,
      );


  static TextStyle get splashText => TextStyle(
        fontFamily: FontFamily.iranYekan,
        fontWeight: FontWeight.w700,
        fontSize: FontSize.s30,
        color: AppColors.lagani,
        inherit: false,
      );



  static TextStyle get titleListViewText => TextStyle(
    fontFamily: FontFamily.iranYekan,
    fontWeight: FontWeight.w700,
    fontSize: FontSize.s22,
    color: AppColors.titleListViewText,
    inherit: false,
  );




}
