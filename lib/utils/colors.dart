import 'package:flutter/material.dart';

class AppColors {
  static const Color base_color = const Color(0xFF9FD225);

  static const Color primary = const Color(0xFF18a096);

  static const Color lagani = const Color(0XFF7AAC00);

  static const Color login_text = const Color(0XFF334705);

  static const Color input_text = const Color(0XFF202020);

  static const Color disabled_btn = const Color(0XFFA5A5A5);

  static const Color white_smoke = const Color(0XFFF6F6F6);

  static const Color titleListViewText = const Color(0XFF242424);




}
