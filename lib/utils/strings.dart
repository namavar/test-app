class AppLabels {
  static const String email = "ایمیل";
  static const String password = "پسورد";
  static const String Login = "تایید";
  static const String firstName = "نام";
  static const String home = "خانه";
}

class AppStrings {
  static const String appName = "تست فلاتر";

  static const String enterEmail = "لطفآ ایمیل خود را وارد کنید";

  static const String enterValidEmail = "Enter Valid Email";

  static const String enterPassword = "لطفآ پسورد را وارد کنید";

  static const String enterFirstName = "لطفآ نام خود را وارد کنید";

  static const String enterLastName = "لطفآ نام خانوادگی خود را وارد کنید";

  static const String enterValidPassword = "پسورد یا نام کاربری  اشتباه هست";

  static const String loginTitle = " نام کاربری و پسورد خود را وارد کرده";

  static const String loginSubTitle = "و پس از تایید منتظر بمانید";

  static const String exit = "خروج";

}
