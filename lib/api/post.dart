import 'package:flutter/material.dart';
import 'package:test_app/components/get_header_token.dart';
import 'package:test_app/components/save_current_login.dart';
import 'package:test_app/components/show_dialog_single_button.dart';
  import 'package:test_app/models/login.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';

class Urls {
  static const BASE_API_URL = "http://10.0.2.2:8000/api/";
}

class AppApi {
  static Future<Login> fetchLogin(Map data) async {
    final response =
        await http.post('${Urls.BASE_API_URL}login', body: data);
    print(response.body);
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      return Login.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }



}
