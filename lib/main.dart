import 'package:flutter/material.dart';

import 'package:test_app/app.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Color(0XFF334705), // status bar color
  ));
  runApp(MaterialApp(
    title: "تست فلاتر",
    debugShowCheckedModeBanner: false,
    home: MyApp(),
  ));
}
