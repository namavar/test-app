import 'package:flutter/material.dart';
import 'package:test_app/ui/screens/splash_screen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        supportedLocales: [
          const Locale('fa', ''), // Persian
        ],
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        locale: Locale('fa'),
        title: 'تست فلاتر',
        debugShowCheckedModeBanner:false,
        home: SplashScreen()
    );
  }
}
