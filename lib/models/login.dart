class Login {

  String access_token;
  String name;
  String email;

  Login({ this.name, this.email, this.access_token});

  factory Login.fromJson(Map<String, dynamic> json) {
    return Login(
      name: json['user']['name'],
      email: json['user']['email'],
      access_token: json['access_token'],

    );
  }

}