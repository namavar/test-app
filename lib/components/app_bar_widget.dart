import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app/utils/colors.dart';
import 'package:test_app/utils/screen_util.dart';
import 'package:test_app/utils/strings.dart';
import 'package:test_app/utils/text_styles.dart';

class AppBarWidget extends AppBar {
  final _formKey = new GlobalKey<FormState>();

  AppBarWidget({Key key})
      : super(
          automaticallyImplyLeading: false,
          key: key,
          backgroundColor: AppColors.base_color,
          title: Padding(
            padding: EdgeInsets.all(Constant.sizeMedium),
            child: Text(AppStrings.appName, style: TextStyles.appName),
          ),
        );
}
