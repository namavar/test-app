import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_app/utils/colors.dart';
import 'package:test_app/utils/screen_util.dart';
import 'package:test_app/utils/text_styles.dart';

class AppTextFormField extends StatelessWidget {
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final String hintText;
  final String lableText;
  final String prefixText;
  final IconData icon;
  final String initialValue;
  final int maxLength;
  final TextInputType keyboardType;
  final bool enabled;
  final bool obscureText;
  final TextInputAction textInputAction;
  final FocusNode focusNode;
  final ValueChanged<String> onFieldSubmitted;

  const AppTextFormField({
    Key key,
    this.onSaved,
    this.validator,
    this.hintText = "",
    this.lableText = "",
    this.prefixText,
    this.maxLength,
    this.icon,
    this.keyboardType,
    this.enabled = true,
    this.obscureText = false,
    this.initialValue,
    this.focusNode,
    this.onFieldSubmitted,
    this.textInputAction = TextInputAction.done,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
//      decoration: BoxDecoration(
//
//          border: Border.all(
//            color: AppColors.primary,
//          ),
//          borderRadius: BorderRadius.all(Radius.circular(Constant.sizeSmall))),
//      padding: EdgeInsets.all(Constant.sizeMedium),
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: TextFormField(
//          focusNode: focusNode,
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            labelText: lableText,
            labelStyle: TextStyles.labelStyle,
            hintText: hintText,
            hintStyle: TextStyles.hintStyle,
            errorStyle: TextStyles.errorStyle,
            contentPadding:
                EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
            prefixText: prefixText,
            errorMaxLines: 1,
//          contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Color(0XFFCBCBCB), width: 2.0)),
          ),

          textAlign: TextAlign.right,
          inputFormatters: (maxLength != null)
              ? obscureText
                  ? [
                      LengthLimitingTextInputFormatter(maxLength),
                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
                    ]
                  : [LengthLimitingTextInputFormatter(maxLength)]
              : [],
          onSaved: onSaved,
          onFieldSubmitted: onFieldSubmitted,
          validator: validator,
          keyboardType: keyboardType,
          enabled: enabled,
          initialValue: initialValue ?? "",
          textInputAction: textInputAction,
          style: TextStyles.editText,
          obscureText: obscureText,
        ),
      ),
    );
  }
}
