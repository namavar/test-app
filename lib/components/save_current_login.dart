import 'package:test_app/models/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

saveCurrentLogin(Map responseJson) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();

  var token = (responseJson != null && !responseJson.isEmpty)
      ? Login.fromJson(responseJson).access_token
      : "";

//  var email = (responseJson != null && !responseJson.isEmpty) ? Active.fromJson(responseJson).email : "";
//  var pk = (responseJson != null && !responseJson.isEmpty) ? Active.fromJson(responseJson).user.id : 0;

//  await preferences.setString('LastUser', (user != null && user.length > 0) ? user : "");
  await preferences.setString(
      'LastToken', (token != null && token.length > 0) ? token : "");
//  await preferences.setString('LastEmail', (email != null && email.length > 0) ? email : "");
//  await preferences.setInt('LastUserId', (pk != null && pk > 0) ? pk : 0);
}
