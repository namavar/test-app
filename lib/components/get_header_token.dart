import 'package:shared_preferences/shared_preferences.dart';

getHeaderToken() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String getToken = await preferences.getString("LastToken");

  return getToken;
}
